#encoding: utf-8

# Jaromír Červenka
# jaromir@virt.io
# https://virt.io/
# License: MIT

require 'open-uri'
require 'bigdecimal'

class CNBRates
  SOURCE_URL = 'http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date='

  attr_reader :currency_list

  def initialize(options = {})
    @date = options[:date] || Date.today
    @filename = options[:filename] || ''
    @big_decimals = options[:big_decimals] || false
    @date = @date.strftime('%d.%m.%Y')
    @currency_list = []

    parse_list
  end

  def rate(currency, amount=1)
    method_name = currency.downcase.to_sym
    rate = if respond_to?(method_name) then
             send(method_name)
           else
             @big_decimals ? BigDecimal.new(1) : 1.0
           end
    rate * amount
  end

  private

  def parse_list
    lines = @filename.empty? ? list_from_web : list_from_file

    lines.each_line do |line|
      split_line = line.split('|')

      volume = @big_decimals ? BigDecimal.new(split_line[2]) : split_line[2].to_f
      currency_code = split_line[3].downcase

      currency_rate_str = split_line[4].gsub(',', '.')
      currency_rate = @big_decimals ? BigDecimal.new(currency_rate_str) : currency_rate_str.to_f
      currency_rate = volume == 1 ? currency_rate : currency_rate / volume

      self.class.send(:define_method, "#{currency_code}") do
        currency_rate
      end

      @currency_list.push(currency_code.to_sym)
    end
  end

  def list_from_file
    lines = ''

    File.open @filename, 'r' do |file|
      file.each_with_index do |line, index|
        next if index == 0 || index == 0
        lines += line
      end
    end

    return lines
  end

  def list_from_web
    lines = ''

    open(source_url) do |doc|
      doc.each_with_index do |line, index|
        next if index == 0 || index == 1
        lines += line
      end
    end

    return lines
  end

  def source_url
    "#{SOURCE_URL}#{@date}"
  end
end

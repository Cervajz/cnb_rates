#encoding: utf-8
require 'test/unit'
require 'cnb_rates'

class CNBRatesTest < Test::Unit::TestCase
  TEMPLATE_FILE = 'test/rates.txt'

  def test_opening_file
    assert_nothing_raised do
      CNBRates.new(filename: TEMPLATE_FILE)
    end
  end

  def test_rate_equality
    assert_equal 20.061, CNBRates.new(filename: TEMPLATE_FILE).usd
  end

  def test_rate_multiplication
    assert_equal 200.61, CNBRates.new(filename: TEMPLATE_FILE).rate(:usd, 10)
  end

  def test_big_decimals
    assert CNBRates.new(filename: TEMPLATE_FILE, big_decimals: true).eur.is_a?(BigDecimal)
  end

  def test_non_existing_currency
    assert_equal 1.0, CNBRates.new(filename: TEMPLATE_FILE).rate(:abcd)
  end
end

cnb_rates
---------

Simple ruby parser for the Czech National Bank exchange rates:

http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt

Usage
-----

```
  gem install cnb_rates
```


```ruby
  require 'cnb_rates'
  
  CNBRates.new.currency_list
  => [:aud, :brl, :bgn, :cny, :dkk, :eur, :php, :hkd, :hrk, :inr, :idr, :ils, :jpy, :zar, :krw, :cad, :ltl, :lvl, :huf,  
      :myr, :mxn, :xdr, :nok, :nzd, :pln, :ron, :rub, :sgd, :sek, :chf, :thb, :try, :usd, :gbp]
  
  CNBRates.new.usd
  => 20.061
  
  CNBRates.new.rate :aud
  => 17.936
  
  CNBRates.new.rate :aud, 10
  => 179.36
  
  CNBRates.new(date: Date.new(2013, 01, 01)).eur
  => 25.14
  
  CNBRates.new(filename: '/Users/cervajz/denni_kurz.txt').eur
  => 27.535

  CNBRates.new(big_decimals: true).usd
  => #<BigDecimal:7fbac8dfcd18,'0.20061E2',18(18)>
````

License
-------
Copyright (C) 2013 Virtio Ltd


Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
Gem::Specification.new do |s|
  s.name        = 'cnb_rates'
  s.version     = '0.0.8'
  s.date        = '2013-12-09'
  s.description = "Simple ruby parser for the Czech National Bank exchange rates"
  s.summary     = s.description
	s.authors     = ["Jaromír Červenka"]
  s.email       = 'jaromir@virt.io'
  s.files       = ["lib/cnb_rates.rb"]
  s.homepage    = 'https://github.com/virtio/cnb_rates'
  s.license     = 'CC BY'
end
